[![coverage report](https://gitlab.com/b10-adprog/vaccindo/badges/master/coverage.svg)](https://gitlab.com/b10-adprog/vaccindo/-/commits/master)

# Group Project Advanced Programming 2021
Kelompok B10:  
1. Mutia Rahmatun Husna (1706039622)  
2. Niti Cahyaning Utami (1906350894)  
3. Gita Permatasari Sujatmiko (1906400053)  
4. Erania Siti Rajisa (1706039401)  
5. Steven Wiryadinata Halim (1906350622)  

## Deskripsi
***Tipe Aplikasi:*** Aplikasi Web  
***Nama Aplikasi:*** VaccIndo  
***Tech Stack:*** Spring Boot + Thymeleaf  
***Deskripsi Aplikasi:***
Merupakan suatu aplikasi berbasis website yang akan membantu masyarakat dalam melakukan pendaftaran vaksinasi dan donasi bantuan Covid-19. Pengguna aplikasi terbagi menjadi tenaga kesehatan sebagai administrator dan user biasa (user, pendaftar) yang merupakan masyarakat yang akan mendaftar vaksinasi ataupun berdonasi.

## Fitur Utama
1. Administrator membuat berita mengenai Vaksinasi maupun Covid-19
2. Administrator menambah daftar Rumah Sakit
3. Administrator menyetujui pendaftaran vaksinasi
4. User biasa mendaftar vaksinasi
5. User biasa melakukan donasi
