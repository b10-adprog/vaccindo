package cs.ui.ac.id.vaccindo.historypendaftaran.controller;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindo.historypendaftaran.service.HistoryPendaftaranService;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = HistoryPendaftaranController.class)
public class HistoryPendaftaranControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HistoryPendaftaranService historyPendaftaranService;

    private HistoryPendaftaran historyPendaftaran;
    private Pendaftar pendaftar;

    @BeforeEach
    public void setUp() {
        pendaftar = new Pendaftar();
        pendaftar.setNama("Gita");
        pendaftar.setNik("1234567890");
        pendaftar.setRegistered(true);

        historyPendaftaran = new HistoryPendaftaran();
        historyPendaftaran.setTanggal("4 April 2021");
        historyPendaftaran.setRumahSakit("RSUI");
        historyPendaftaran.setPendaftar(pendaftar);
    }

    @Test
    public void getHPUrlByGetAllUnfinishedHistoryPendaftaran() throws Exception{
        mockMvc.perform(get("/pendaftaran/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getListPendaftaran"))
                .andExpect(model().attributeExists("unfinishedPendaftaran"))
                .andExpect(view().name("historypendaftaran/listPendaftaran"));
        verify(historyPendaftaranService, times(1)).getListPendaftaranUnfinished();
    }

    @Test
    public void testDaftarVaksinasi() throws Exception{
        mockMvc.perform(get("/daftar-vaksinasi/"))
                .andExpect(status().isOk())
                .andExpect(view().name("historypendaftaran/daftarVaksinasi"));
    }

    @Test
    public void testPostPendaftaranSetuju() throws Exception{
        mockMvc.perform(post("/pendaftaran/" + historyPendaftaran.getIdHistoryPendaftaran()).param("setuju", "setuju"))
                .andExpect(handler().methodName("postSetujui"))
                .andExpect(redirectedUrl("/pendaftaran"));
        verify(historyPendaftaranService, times(1)).updateApprovalStatus(0, true);
        verify(historyPendaftaranService, times(1)).updateFinishedStatus(0, true);
    }

    @Test
    public void testPostPendaftaranTolak() throws Exception{
        mockMvc.perform(post("/pendaftaran/" + historyPendaftaran.getIdHistoryPendaftaran()).param("tolak", "tolak"))
                .andExpect(handler().methodName("postTolak"))
                .andExpect(redirectedUrl("/pendaftaran"));
        verify(historyPendaftaranService, times(1)).updateApprovalStatus(0, false);
        verify(historyPendaftaranService, times(1)).updateFinishedStatus(0, true);
    }

    @Test
    public void testPostDaftarVaksinasi() throws Exception{
        mockMvc.perform(post("/daftar-vaksinasi"))
                .andExpect(handler().methodName("postPendaftaran"))
                .andExpect(redirectedUrl("/pendaftaran"));
    }

    @Test
    public void testProfile() throws Exception{
        mockMvc.perform(get("/profile/" + pendaftar.getNik()))
                .andExpect(handler().methodName("profile"))
                .andExpect(model().attributeExists("allHistoryPendaftaran"))
                .andExpect(view().name("historypendaftaran/profile"));
        verify(historyPendaftaranService, times(1)).getAllHistoryPendaftaranOfAProfile(pendaftar.getNik());
    }

}
