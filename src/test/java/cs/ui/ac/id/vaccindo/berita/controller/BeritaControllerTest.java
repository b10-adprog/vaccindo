package cs.ui.ac.id.vaccindo.berita.controller;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindo.berita.service.BeritaService;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {BeritaControllerUser.class, BeritaControllerAdmin.class})
public class BeritaControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    PendaftarService pendaftarService;

    @MockBean
    private BeritaService beritaService;

    private Pendaftar pendaftar;
    private Berita beritaNasional;
    private Berita beritaInternasional;

    @BeforeEach
    public void setUp() {
        pendaftar = new Pendaftar("1906350894", "Nitami");
        pendaftar.setSubbedToBeritaInternasional(false);
        pendaftar.setSubbedToBeritaNasional(true);
        pendaftar.setRegistered(true);

        beritaNasional = new Berita();
        beritaNasional.setIdBerita(1);
        beritaNasional.setJudul("Judul");
        beritaNasional.setKategori(KategoriBerita.NASIONAL);
        beritaNasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaNasional.setDeskripsi("Deskripsi");
        beritaNasional.setUrlGambar("url//ke/sesuatu.png");

        beritaInternasional = new Berita();
        beritaInternasional.setIdBerita(2);
        beritaInternasional.setJudul("Title");
        beritaInternasional.setKategori(KategoriBerita.INTERNASIONAL);
        beritaInternasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaInternasional.setDeskripsi("Description");
        beritaInternasional.setUrlGambar("url//to/someting.png");
    }

    @Test
    public void getBeritaUrlByPenggunaCallGetAllBerita() throws Exception{
        mockMvc.perform(get("/1906350894/berita"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaPengguna"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(model().attributeExists("nik"))
                .andExpect(view().name("berita/lihatBerita"));
        verify(beritaService, times(1)).getAllBerita(any(String.class));
    }

    @Test
    public void getLihatBeritaUrlByPenggunaCallGetAllBerita() throws Exception{
        mockMvc.perform(get("/1906350894/berita/lihat"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaPengguna"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(model().attributeExists("nik"))
                .andExpect(view().name("berita/lihatBerita"));
        verify(beritaService, times(1)).getAllBerita(any(String.class));
    }

    @Test
    public void getUbahLanggananBeritaUrlByPenggunaReturnUbahLanggananView() throws Exception{
        when(pendaftarService.getPendaftarByNik(any(String.class))).thenReturn(pendaftar);
        mockMvc.perform(get("/1906350894/berita/ubah-langganan"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("ubahLanggananPengguna"))
                .andExpect(model().attributeExists("pendaftar"))
                .andExpect(view().name("berita/ubahLangganan"));
    }

    @Test
    public void postUbahLanggananBeritaUrlByPenggunaRedirectToBerita() throws Exception{
        when(pendaftarService.getPendaftarByNik(any(String.class))).thenReturn(pendaftar);

        mockMvc.perform(post("/1906350894/berita/ubah-langganan")
                    .param("NasionalSubOptions", "false")
                    .param("InternasionalSubOptions", "false")
                )
                .andExpect(handler().methodName("ubahLanggananPenggunaPost"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/1906350894/berita"));
    }

    @Test
    public void getLihatDetailBeritaValidUrlByPenggunaCallGetBerita() throws Exception{
        assertEquals("2020-11-01 12:00", beritaNasional.getTimeStamp());
        when(beritaService.getBerita(any(String.class), any(Integer.class))).thenReturn(beritaNasional);

        mockMvc.perform(get("/1906350894/berita/lihat/detail/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("detailBeritaPengguna"))
                .andExpect(model().attributeExists("berita"))
                .andExpect(view().name("berita/detailBerita"));
        verify(beritaService, times(1)).getBerita(any(String.class), any(Integer.class));
    }

    @Test
    public void getLihatDetailBeritaInvalidUrlByPenggunaCallGetBerita() throws Exception{
        mockMvc.perform(get("/1906350894/berita/lihat/detail/judul"))
                .andExpect(status().isNotFound());
        verify(beritaService, times(0)).getBerita(any(String.class),any(Integer.class));
    }

    @Test
    public void getTulisBeritaUrlByAdminReturnsFormPage() throws Exception{
        Berita berita = new Berita();
        mockMvc.perform(get("/admin/berita/tulis/")
                    .param("berita", String.valueOf(berita))
                ).andExpect(status().isOk())
                .andExpect(model().attributeExists("berita"))
                .andExpect(handler().methodName("tulisBeritaAdmin"))
                .andExpect(view().name("berita/tulisBerita")
                );
    }

    @Test
    public void postTulisBeritaUrlByAdminRedirectsToListBeritaPage() throws Exception{
        Berita berita = new Berita("Berita nasional", KategoriBerita.NASIONAL, "url//to/someting.png", "Deskripsi");
        berita.setIdBerita(1);
        berita.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        assertNotNull(berita);

        List<Berita> listBerita = new ArrayList<>();
        listBerita.add(berita);

        when(beritaService.getAllBerita()).thenReturn(listBerita);

        mockMvc.perform(post("/admin/berita/save/")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("judul", berita.getJudul())
                    .param("kategori", "NASIONAL")
                    .param("deskripsi", berita.getDeskripsi())
                ).andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("tulisBeritaAdmin"))
                .andExpect(redirectedUrl("/admin/berita/lihat/"));
        verify(beritaService, times(1)).addBerita(any(Berita.class));
    }
    @Test
    public void postTulisBeritaUrlInvalidByAdminReturnsBeritaPage() throws Exception{
        mockMvc.perform(post("/admin/berita/save/")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_FORM_URLENCODED)
                ).andExpect(handler().methodName("tulisBeritaAdmin"))
                .andExpect(view().name("berita/tulisBerita"));
        verify(beritaService, times(0)).addBerita(any(Berita.class));
    }

    @Test
    public void getListBeritaUrlByAdminCallGetAllBerita() throws Exception{
        mockMvc.perform(get("/admin/berita/lihat"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaAdmin"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(view().name("berita/listBerita"));
        verify(beritaService, times(1)).getAllBerita();
    }

    @Test
    public void getBeritaUrlByAdminCallGetAllBerita() throws Exception{
        mockMvc.perform(get("/admin/berita"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaAdmin"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(view().name("berita/listBerita"));
        verify(beritaService, times(1)).getAllBerita();
    }

    @Test
    public void deleteBeritaUrlByAdminThenRedirectToListBeritaPage() throws Exception{
        mockMvc.perform(get("/admin/berita/delete/1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("deleteBeritaAdmin"))
                .andExpect(redirectedUrl("/admin/berita/lihat"));
        verify(beritaService, times(1)).deleteBerita(any(Integer.class));
    }
}
