package cs.ui.ac.id.vaccindo.historypendaftaran.service;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindo.historypendaftaran.repository.HistoryPendaftaranRepository;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class HistoryPendaftaranImplTest {
    @Mock
    private HistoryPendaftaranRepository historyPendaftaranRepository;

    @Mock
    private PendaftarServiceImpl pendaftarService;

    @InjectMocks
    private HistoryPendaftaranServiceImpl historyPendaftaranService;

    private Pendaftar pendaftar;
    private Pendaftar pendaftar2;
    private HistoryPendaftaran historyPendaftaran;
    private HistoryPendaftaran historyPendaftaran2;
    private List<HistoryPendaftaran> historyPendaftaranListTotal;
    private List<HistoryPendaftaran> historyPendaftaranList1;
    private List<HistoryPendaftaran> historyPendaftaranList2;

    @BeforeEach
    public void setUp() {
        pendaftar = new Pendaftar();
        pendaftar.setNama("Gita");
        pendaftar.setNik("1234567890");
        pendaftar.setRegistered(true);

        pendaftar2 = new Pendaftar();
        pendaftar2.setNama("Atig");
        pendaftar2.setNik("0987654321");
        pendaftar2.setRegistered(true);

        historyPendaftaran = new HistoryPendaftaran();
        historyPendaftaran.setTanggal("4 April 2021");
        historyPendaftaran.setRumahSakit("RSUI");
        historyPendaftaran.setPendaftar(pendaftar);

        historyPendaftaran2 = new HistoryPendaftaran();
        historyPendaftaran2.setTanggal("6 April 2021");
        historyPendaftaran2.setRumahSakit("RS Permata Indah");
        historyPendaftaran2.setPendaftar(pendaftar2);

        historyPendaftaranListTotal = new ArrayList<>();
        historyPendaftaranListTotal.add(historyPendaftaran);
        historyPendaftaranListTotal.add(historyPendaftaran2);

        historyPendaftaranList1 = new ArrayList<>();
        historyPendaftaranList1.add(historyPendaftaran);

        historyPendaftaranList2 = new ArrayList<>();
        historyPendaftaranList2.add(historyPendaftaran2);

    }

    @Test
    public void testHPServiceCreateHistoryPendaftaran(){
        lenient().when(historyPendaftaranService.createHistoryPendaftaran(historyPendaftaran)).thenReturn(historyPendaftaran);
    }

    @Test
    public void testHPServiceGetAllHP() {
        when(historyPendaftaranService.getAllHistoryPendaftaran()).thenReturn(historyPendaftaranListTotal);

        List<HistoryPendaftaran> listHPFromMock = historyPendaftaranService.getAllHistoryPendaftaran();

        assertEquals(listHPFromMock.size(), 2);
        verify(historyPendaftaranRepository, times(1)).findAll();
    }

    @Test
    public void testHPServiceGetAllHPOfAProfile() {
        when(historyPendaftaranService.getAllHistoryPendaftaranOfAProfile(pendaftar.getNik())).thenReturn(historyPendaftaranList1);

        List<HistoryPendaftaran> listHPFromMock = historyPendaftaranService.getAllHistoryPendaftaranOfAProfile(pendaftar.getNik());

        assertEquals(listHPFromMock.size(), 1);
        verify(historyPendaftaranRepository, times(1)).findAll();
    }

    @Test
    public void testHPServiceGetHistoryPendaftaranReturnsCorrectHP() {
        lenient().when(historyPendaftaranService.getHistoryPendaftaran(1)).thenReturn(historyPendaftaran);
        lenient().when(historyPendaftaranService.getHistoryPendaftaran(2)).thenReturn(historyPendaftaran2);

        HistoryPendaftaran HPFromMock1 = historyPendaftaranService.getHistoryPendaftaran(1);
        HistoryPendaftaran HPFromMock2 = historyPendaftaranService.getHistoryPendaftaran(2);

        assertEquals(HPFromMock1.getPendaftar(), pendaftar);
        assertEquals(HPFromMock2.getPendaftar(), pendaftar2);

        verify(historyPendaftaranRepository, times(1)).findByIdHistoryPendaftaran(1);
        verify(historyPendaftaranRepository, times(1)).findByIdHistoryPendaftaran(2);
    }

    @Test
    public void testHPServiceUpdateApprovalStatus() {
        when(historyPendaftaranRepository.findByIdHistoryPendaftaran(1)).thenReturn(historyPendaftaran);
        lenient().when(pendaftarService.getPendaftarByNik(pendaftar.getNik())).thenReturn(pendaftar);

        historyPendaftaranService.updateApprovalStatus(1, false);

        assertFalse(historyPendaftaran.getApproved());
        assertFalse(pendaftar.isFirstVaccinated());

        historyPendaftaranService.updateApprovalStatus(1, true);
        assertTrue(historyPendaftaran.getApproved());
        assertTrue(pendaftar.isFirstVaccinated());
        assertFalse(pendaftar.isSecondVaccinated());

//        historyPendaftaranService.updateApprovalStatus(1, true);
//        assertTrue(pendaftar.isFirstVaccinated());
//        assertTrue(pendaftar.isSecondVaccinated());
    }

    @Test
    public void testHPServiceUpdateFinishedStatus() {
        when(historyPendaftaranRepository.findByIdHistoryPendaftaran(2)).thenReturn(historyPendaftaran2);
        lenient().when(pendaftarService.getPendaftarByNik(pendaftar2.getNik())).thenReturn(pendaftar2);

        historyPendaftaranService.updateFinishedStatus(2, true);

        assertTrue(historyPendaftaran2.isFinished());
    }

    @Test
    public void testHPServiceGetUnfinishedPendaftaran(){
        lenient().when(historyPendaftaranService.getListPendaftaranUnfinished()).thenReturn(historyPendaftaranList1);
    }

    @Test
    public void testHPServiceDaftarVaksinasi(){
        HistoryPendaftaran newHP = new HistoryPendaftaran();
        newHP.setPendaftar(pendaftar);
        newHP.setTanggal("4 April 2020");
        newHP.setRumahSakit("RSSS");

        lenient().when(pendaftarService.getPendaftarByNik(pendaftar.getNik())).thenReturn(pendaftar);

        historyPendaftaranService.daftarVaksinasi(newHP);

        lenient().when(historyPendaftaranRepository.findByIdHistoryPendaftaran(3)).thenReturn(newHP);
    }

//    @Test
//    public void testGetIdPendaftar() {
//        HistoryPendaftaran newHP = new HistoryPendaftaran();
//        newHP.setPendaftar(pendaftar);
//        newHP.setTanggal("4 April 2020");
//        newHP.setRumahSakit("RSSS");
//
//        String nik = pendaftar.getNik();
//        assertEquals(nik, newHP.getIdPendaftar());
//    }

}
