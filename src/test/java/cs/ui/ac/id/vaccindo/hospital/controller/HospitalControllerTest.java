package cs.ui.ac.id.vaccindo.hospital.controller;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.hospital.model.Hospital;
import cs.ui.ac.id.vaccindo.hospital.service.HospitalService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = HospitalController.class)
public class HospitalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HospitalService hospitalService;

    @Test
    public void getInputHospitalFormPage() throws Exception {
        Hospital hospital = new Hospital();

        mockMvc.perform(get("/admin/hospital/input/")
        .param("hospital", String.valueOf(hospital))
        ).andExpect(status().isOk())
                .andExpect(model().attributeExists("hospital"))
                .andExpect(handler().methodName("inputHospital"))
                .andExpect(view().name("hospital/inputHospital"));
    }

    @Test
    public void postInputHospitalRedirectToListHospitalPage() throws Exception {

        Hospital hospital = new Hospital("RS Citra Medika", "Jalan Raya Lenteng Agung no. 54", "Deskripsi");
        hospital.setIdHospital(1);
        assertNotNull(hospital);

        List<Hospital> hospitalList = new ArrayList<>();
        hospitalList.add(hospital);

        when(hospitalService.getAllHospital()).thenReturn(hospitalList);

        mockMvc.perform(post("/admin/hospital/inputSave/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .param("hospitalName", hospital.getHospitalName())
                .param("hospitalAddress", hospital.getHospitalAddress())
        ).andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("inputHospital"))
                .andExpect(redirectedUrl("/admin/hospital/"));
        verify(hospitalService, times(1)).addHospital(any(Hospital.class));
    }

    @Test
    public void getAllListHospital() throws Exception {
        mockMvc.perform(get("/admin/hospital/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("listHospital"))
                .andExpect(model().attributeExists("listHospital"))
                .andExpect(view().name("hospital/listHospital"));
        verify(hospitalService, times(1)).getAllHospital();
    }

}
