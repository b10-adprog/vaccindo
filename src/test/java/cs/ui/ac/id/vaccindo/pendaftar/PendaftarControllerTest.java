package cs.ui.ac.id.vaccindo.pendaftar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cs.ui.ac.id.vaccindo.pendaftar.controller.PendaftarController;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PendaftarController.class)
public class PendaftarControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PendaftarServiceImpl pendaftarService;

    private Pendaftar pendaftar;

    @BeforeEach
    public void setUp(){
        pendaftar = new Pendaftar("12345678", "StevenWH");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerPostMahasiswa() throws Exception{

        when(pendaftarService.createPendaftar(pendaftar)).thenReturn(pendaftar);

        mvc.perform(post("/pendaftar")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(pendaftar)))
                .andExpect(jsonPath("$.nik").value("12345678"));
    }

    @Test
    public void testControllerGetListPendaftar() throws Exception{

        Iterable<Pendaftar> listPendaftar = Arrays.asList(pendaftar);
        when(pendaftarService.getListPendaftar()).thenReturn(listPendaftar);

        mvc.perform(get("/pendaftar").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nik").value("12345678"));
    }

}

