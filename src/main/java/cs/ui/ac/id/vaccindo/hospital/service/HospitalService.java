package cs.ui.ac.id.vaccindo.hospital.service;

import cs.ui.ac.id.vaccindo.hospital.model.Hospital;

import java.util.List;

public interface HospitalService {
    public List<Hospital> getAllHospital();
    public Hospital getHospitalById(Integer idHospital);
    public Hospital getHospitalByName(String name);
    public void addHospital(Hospital hospital);
}
