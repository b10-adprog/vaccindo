package cs.ui.ac.id.vaccindo.pendaftar.repository;

import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendaftarRepository extends JpaRepository<Pendaftar, String> {
    Pendaftar findByNik(String nik);
}