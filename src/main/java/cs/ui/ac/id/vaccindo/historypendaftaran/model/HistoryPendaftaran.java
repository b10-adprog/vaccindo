package cs.ui.ac.id.vaccindo.historypendaftaran.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.repository.PendaftarRepository;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="HistoryPendaftaran")
@Data
@NoArgsConstructor
public class HistoryPendaftaran {

    @Id
    @GeneratedValue
    @Column(name = "idHistoryPendaftaran", updatable = false, nullable = false)
    private int idHistoryPendaftaran;

    @Column(name = "tanggal")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String tanggal;

    @Column(name = "nik")
    private String nik;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pendaftar_id", foreignKey = @ForeignKey(name="pendaftar_id_fkey"))
    private Pendaftar pendaftar;

    @Column(name = "rumah_sakit")
    private String rumahSakit;

    @Column(name = "finished")
    private boolean finished;

    @Column(name = "approved")
    private boolean approved;

    public HistoryPendaftaran(String tanggal, String nik, String rumahSakit){
        this.tanggal = tanggal;
        this.nik = nik;
        this.pendaftar = null;
        this.rumahSakit = rumahSakit;
        this.finished = false;
        this.approved = false;
    }

    public String getIdPendaftar() {
        return pendaftar.getNik();
    }

    public String getNik() {
        return this.nik;
    }

    public void setPendaftar(Pendaftar pendaftar) {
        this.pendaftar = pendaftar;
    }

    public String getNamaPendaftar(){
        return pendaftar.getNama();
    }

    public String getRumahSakit(){
        return this.rumahSakit;
    }

    public String getTanggal() {
        return this.tanggal;
    }

    public boolean getApproved(){
        return this.approved;
    }

    public void setApproved(boolean isApproved) {
        this.approved = isApproved;
    }

    public void setFinished(boolean isFinished) {
        this.finished = isFinished;
    }
}