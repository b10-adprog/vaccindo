package cs.ui.ac.id.vaccindo.berita.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "berita")
@Data
@NoArgsConstructor
public class Berita {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    @Column(name = "id_berita")
    private Integer idBerita;

    @Column(name = "kategori")
    private KategoriBerita kategori;

    @NotEmpty(message = "Judul berita tidak boleh kosong.")
    @Column(updatable = false, nullable = false)
    private String judul;

    @Column(updatable = false)
    private LocalDateTime timeStamp;

    @Column(name = "url_gambar", updatable = false)
    private String urlGambar;

    @NotEmpty(message = "Deskripsi tidak boleh kosong.")
    @Column(updatable = false, nullable = false, columnDefinition = "varchar")
    private String deskripsi;

    public Berita(String judul, KategoriBerita kategori, String urlGambar, String deskripsi) {
        this.judul = judul;
        this.kategori = kategori;
        this.urlGambar = urlGambar;
        this.deskripsi = deskripsi;
    }

    public void setTimeStamp(LocalDateTime now) {
        LocalDateTime dateTimeToMinutes = now.truncatedTo(ChronoUnit.MINUTES);
        this.timeStamp = dateTimeToMinutes;
    }

    public String getTimeStamp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return timeStamp == null ? "-" : timeStamp.format(formatter);
    }
}
