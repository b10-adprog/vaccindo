package cs.ui.ac.id.vaccindo.hospital.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "hospital")
@Data
@NoArgsConstructor
public class Hospital {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_hospital")
    private Integer idHospital;

    @NotEmpty(message = "Nama rumah sakit tidak boleh kosong.")
    @Column(updatable = false, nullable = false)
    private String hospitalName;

    @NotEmpty(message = "Alamat tidak boleh kosong.")
    @Column(updatable = false, nullable = false, columnDefinition = "varChar")
    private String hospitalAddress;

    @Column(updatable = false)
    private String hospitalDescription;

    public Hospital(String hospitalName, String hospitalAddress, String hospitalDescription) {
        this.hospitalName = hospitalName;
        this.hospitalAddress = hospitalAddress;
        this.hospitalDescription = hospitalDescription;
    }
}
