package cs.ui.ac.id.vaccindo.pendaftar.controller;


import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindo.historypendaftaran.service.HistoryPendaftaranService;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PendaftarController {

    @Autowired
    PendaftarService pendaftarService;

    @PostMapping(path = "/pendaftar", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postMahasiswa(@RequestBody Pendaftar pendaftar) {
        return ResponseEntity.ok(pendaftarService.createPendaftar(pendaftar));
    }

    @GetMapping(path = "/pendaftar", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Pendaftar>> getListPendaftar() {
        return ResponseEntity.ok(pendaftarService.getListPendaftar());
    }

}
