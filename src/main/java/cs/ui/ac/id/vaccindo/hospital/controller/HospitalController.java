package cs.ui.ac.id.vaccindo.hospital.controller;

import cs.ui.ac.id.vaccindo.hospital.model.Hospital;
import cs.ui.ac.id.vaccindo.hospital.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class HospitalController {

    @Autowired
    HospitalService hospitalService;

    @GetMapping(path = "/admin/hospital/input/")
    public String inputHospital(Model model) {
        model.addAttribute("hospital", new Hospital());
        return "hospital/inputHospital";
    }

    @PostMapping(path = "/admin/hospital/inputSave/")
    public String inputHospital(@Valid @ModelAttribute(name = "hospital") Hospital hospital, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "hospital/inputHospital";
        }
        hospitalService.addHospital(hospital);
        return "redirect:/admin/hospital/";
    }

    @GetMapping(path = "/admin/hospital/")
    public String listHospital(Model model) {
        List<Hospital> hospitalList = hospitalService.getAllHospital();

        model.addAttribute("listHospital", hospitalList);
        return "hospital/listHospital";
    }
}
