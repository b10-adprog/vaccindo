package cs.ui.ac.id.vaccindo.berita.controller;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.berita.service.BeritaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/admin/berita")
public class BeritaControllerAdmin {
    @Autowired
    BeritaService beritaService;

    @GetMapping(path = {"", "/lihat"})
    public String lihatBeritaAdmin(Model model) {
        List<Berita> listBerita = beritaService.getAllBerita();
        model.addAttribute("listBerita", listBerita);
        return "berita/listBerita";
    }

    @GetMapping(path = "/tulis")
    public String tulisBeritaAdmin(Model model) {
        model.addAttribute("berita", new Berita());
        return "berita/tulisBerita";
    }

    @PostMapping(path = "/save")
    public String tulisBeritaAdmin(@Valid @ModelAttribute(name = "berita") Berita berita,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "berita/tulisBerita";
        }
        berita.setTimeStamp(LocalDateTime.now());
        beritaService.addBerita(berita);
        return "redirect:/admin/berita/lihat/";
    }


    @GetMapping(path = "/delete/{idBerita}")
    public String deleteBeritaAdmin(@PathVariable Integer idBerita) {
        beritaService.deleteBerita(idBerita);
        return "redirect:/admin/berita/lihat";
    }

}
