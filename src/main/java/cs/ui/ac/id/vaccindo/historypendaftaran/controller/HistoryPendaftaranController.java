package cs.ui.ac.id.vaccindo.historypendaftaran.controller;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindo.historypendaftaran.service.HistoryPendaftaranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class HistoryPendaftaranController {
    @Autowired
    HistoryPendaftaranService historyPendaftaranService;

    @GetMapping(path = "/pendaftaran")
    public String getListPendaftaran(Model model){
        List<HistoryPendaftaran> unfinishedPendaftaran = historyPendaftaranService.getListPendaftaranUnfinished();
        model.addAttribute("unfinishedPendaftaran", unfinishedPendaftaran);
        return "historypendaftaran/listPendaftaran";
    }

    @PostMapping(path = "/pendaftaran/{idHistoryPendaftaran}", params = "setuju")
    public String postSetujui(HttpServletRequest request, @PathVariable int idHistoryPendaftaran) {
        historyPendaftaranService.updateApprovalStatus(idHistoryPendaftaran, true);
        historyPendaftaranService.updateFinishedStatus(idHistoryPendaftaran, true);
        return "redirect:/pendaftaran";
    }

    @PostMapping(path = "/pendaftaran/{idHistoryPendaftaran}", params = "tolak")
    public String postTolak(HttpServletRequest request, @PathVariable int idHistoryPendaftaran) {
        historyPendaftaranService.updateApprovalStatus(idHistoryPendaftaran, false);
        historyPendaftaranService.updateFinishedStatus(idHistoryPendaftaran, true);
        return "redirect:/pendaftaran";
    }

    @GetMapping(path="/daftar-vaksinasi")
    public String getFormPendaftaran(Model model) {
        model.addAttribute("historyPendaftaran", new HistoryPendaftaran());
        return "historypendaftaran/daftarVaksinasi";
    }

    @PostMapping(path="/daftar-vaksinasi")
    public String postPendaftaran(@ModelAttribute(name = "historyPendaftaran") HistoryPendaftaran historyPendaftaran) {
        historyPendaftaran.setApproved(false);
        historyPendaftaran.setFinished(false);
        historyPendaftaranService.daftarVaksinasi(historyPendaftaran);
        return "redirect:/pendaftaran";
    }

    @GetMapping(path = "/profile/{idProfile}")
    public String profile(Model model, @PathVariable String idProfile) {
        List<HistoryPendaftaran> allHistoryPendaftaran =
                historyPendaftaranService.getAllHistoryPendaftaranOfAProfile(idProfile);
        model.addAttribute("allHistoryPendaftaran", allHistoryPendaftaran);
        return "historypendaftaran/profile";
    }
}
