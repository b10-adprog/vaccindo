package cs.ui.ac.id.vaccindo.pendaftar.service;

import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.repository.PendaftarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PendaftarServiceImpl implements  PendaftarService{
    @Autowired
    private PendaftarRepository pendaftarRepository;

    @Override
    public Pendaftar createPendaftar(Pendaftar pendaftar){
        pendaftarRepository.save(pendaftar);
        return pendaftar;
    }

    @Override
    public Iterable<Pendaftar> getListPendaftar() {
        return pendaftarRepository.findAll();
    }

    @Override
    public Pendaftar getPendaftarByNik(String nik) {
        return pendaftarRepository.findByNik(nik);
    }

}
