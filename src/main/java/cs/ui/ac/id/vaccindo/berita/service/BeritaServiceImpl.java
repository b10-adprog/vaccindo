package cs.ui.ac.id.vaccindo.berita.service;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindo.berita.repository.BeritaRepository;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BeritaServiceImpl implements BeritaService {
    @Autowired
    BeritaRepository beritaRepo;
    @Autowired
    PendaftarService pendaftarService;

    @Override
    public Berita getBerita(Integer idBerita) {
        return beritaRepo.findBeritaByIdBerita(idBerita);
    }

    @Override
    public List<Berita> getAllBerita() {
        return beritaRepo.findAll();
    }

    @Override
    public Berita getBerita(String nik_user, Integer idBerita) {
        return beritaRepo.findBeritaByIdBerita(idBerita);
    }

    @Override
    public List<Berita> getAllBerita(String nik_user) {
        Pendaftar user = pendaftarService.getPendaftarByNik(nik_user);
        List<Berita> listBerita = new ArrayList<>();
        if (user.isSubbedToBeritaNasional()) {
            List<Berita> listBeritaNasional = beritaRepo.findBeritasByKategoriIs(KategoriBerita.NASIONAL);
            listBerita.addAll(listBeritaNasional);
        }
        if (user.isSubbedToBeritaInternasional()) {
            List<Berita> listBeritaInternasional = beritaRepo.findBeritasByKategoriIs(KategoriBerita.INTERNASIONAL);
            listBerita.addAll(listBeritaInternasional);
        }
        return listBerita;
    }


    @Override
    @Transactional
    public void deleteBerita(Integer idBerita) {
        beritaRepo.deleteBeritaByIdBerita(idBerita);
    }

    @Override
    @Transactional
    public void addBerita(Berita berita) {
        beritaRepo.save(berita);
    }
}
