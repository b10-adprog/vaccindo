package cs.ui.ac.id.vaccindo.historypendaftaran.repository;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryPendaftaranRepository extends JpaRepository<HistoryPendaftaran, Integer> {
    HistoryPendaftaran findByIdHistoryPendaftaran(int id);

    List<HistoryPendaftaran> findAllByFinishedIs(boolean finished);
}
