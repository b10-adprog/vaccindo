package cs.ui.ac.id.vaccindo.berita.service;

import cs.ui.ac.id.vaccindo.berita.model.Berita;

import java.util.List;

public interface BeritaService {
    public List<Berita> getAllBerita();
    public Berita getBerita(Integer idBerita);
    public List<Berita> getAllBerita(String nik_user);
    public Berita getBerita(String nik_user, Integer idBerita);
    public void deleteBerita(Integer idBerita);
    public void addBerita(Berita berita);
}
