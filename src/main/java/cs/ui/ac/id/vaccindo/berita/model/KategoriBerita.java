package cs.ui.ac.id.vaccindo.berita.model;

public enum KategoriBerita {
    NASIONAL,
    INTERNASIONAL
}
