package cs.ui.ac.id.vaccindo.berita.controller;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.berita.service.BeritaService;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/{nik_user:[\\d]+}/berita")
public class BeritaControllerUser {
    @Autowired
    BeritaService beritaService;

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path = {"", "/lihat"})
    public String lihatBeritaPengguna(Model model,
                                      @PathVariable String nik_user) {
        List<Berita> listBerita = beritaService.getAllBerita(nik_user);
        model.addAttribute("listBerita", listBerita);
        model.addAttribute("nik", nik_user);
        return "berita/lihatBerita";
    }

    @GetMapping(path = {"/ubah-langganan"})
    public String ubahLanggananPengguna(Model model, @PathVariable String nik_user) {
        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(nik_user);
        model.addAttribute("pendaftar", pendaftar);
        return "berita/ubahLangganan";
    }

    @PostMapping(path = {"/ubah-langganan"})
    public String ubahLanggananPenggunaPost(@PathVariable String nik_user,
                                            @RequestParam(name = "NasionalSubOptions") String nasional,
                                            @RequestParam(name = "InternasionalSubOptions") String internasional) {
        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(nik_user);
        pendaftar.setSubbedToBeritaNasional(Boolean.parseBoolean(nasional));
        pendaftar.setSubbedToBeritaInternasional(Boolean.parseBoolean(internasional));
        pendaftarService.createPendaftar(pendaftar);
        return "redirect:/{nik_user}/berita";
    }

    @GetMapping(path = "/lihat/detail/{idBerita:[\\d]+}")
    public String detailBeritaPengguna(Model model,
                                       @PathVariable String nik_user,
                                       @PathVariable Integer idBerita) {
        Berita berita = beritaService.getBerita(nik_user, idBerita);
        model.addAttribute("berita", berita);
        return "berita/detailBerita";
    }

}
