package cs.ui.ac.id.vaccindo.historypendaftaran.service;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;

import java.util.List;

public interface HistoryPendaftaranService {
    HistoryPendaftaran createHistoryPendaftaran(HistoryPendaftaran history);
    List<HistoryPendaftaran> getAllHistoryPendaftaran();
    List<HistoryPendaftaran> getAllHistoryPendaftaranOfAProfile(String idProfile);
    HistoryPendaftaran getHistoryPendaftaran(Integer idHistoryPendaftaran);
    List<HistoryPendaftaran> getListPendaftaranUnfinished();
    void updateApprovalStatus(int idHistoryPendaftaran, boolean isVaccinated);
    void updateFinishedStatus(int idHistoryPendaftaran, boolean isVaccinated);
    public void daftarVaksinasi(HistoryPendaftaran historyPendaftaran);
}
