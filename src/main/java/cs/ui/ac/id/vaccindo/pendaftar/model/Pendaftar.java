package cs.ui.ac.id.vaccindo.pendaftar.model;

import cs.ui.ac.id.vaccindo.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="pendaftar")
@Data
@NoArgsConstructor
public class Pendaftar {

    @Id
    @Column(name = "nik", updatable = false, nullable = false)
    private String nik;

    @Column(name = "nama")
    private String nama;

    @Column(name = "registered")
    private boolean registered;

    @Column(name = "firstVaccinated")
    private boolean firstVaccinated;

    @Column(name = "secondVaccinated")
    private boolean secondVaccinated;

    @OneToMany(cascade = CascadeType.ALL)
    private List<HistoryPendaftaran> historyPendaftarans = new ArrayList<>();

    @Column(name = "subbedToBeritaNasional")
    private boolean isSubbedToBeritaNasional;

    @Column(name = "subbedToBeritaInternasional")
    private boolean isSubbedToBeritaInternasional;

    public Pendaftar(String nik, String nama){
        this.nik = nik;
        this.nama = nama;
        this.registered = false;
        this.firstVaccinated = false;
        this.secondVaccinated = false;
    }

    public String getNama() {
        return nama;
    }

    public String getNik() {
        return nik;
    }
}
