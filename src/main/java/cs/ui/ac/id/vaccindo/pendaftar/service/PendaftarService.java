package cs.ui.ac.id.vaccindo.pendaftar.service;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;

public interface PendaftarService {

    Pendaftar createPendaftar(Pendaftar pendaftar);

    Iterable<Pendaftar> getListPendaftar();

    Pendaftar getPendaftarByNik(String nik);
}
