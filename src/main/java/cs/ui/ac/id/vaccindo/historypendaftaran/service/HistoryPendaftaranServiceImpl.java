package cs.ui.ac.id.vaccindo.historypendaftaran.service;

import cs.ui.ac.id.vaccindo.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindo.historypendaftaran.repository.HistoryPendaftaranRepository;
import cs.ui.ac.id.vaccindo.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindo.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindo.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryPendaftaranServiceImpl implements HistoryPendaftaranService{

    @Autowired
    HistoryPendaftaranRepository historyPendaftaranRepository;

    @Autowired
    PendaftarService pendaftarService;

    @Override
    public HistoryPendaftaran createHistoryPendaftaran(HistoryPendaftaran history) {
        historyPendaftaranRepository.save(history);
        return history;
    }

    @Override
    public List<HistoryPendaftaran> getAllHistoryPendaftaran() {
        return historyPendaftaranRepository.findAll();
    }

    @Override
    public List<HistoryPendaftaran> getAllHistoryPendaftaranOfAProfile(String idProfile) {
        List<HistoryPendaftaran> historyPendaftaranList = new ArrayList<>();
        for (HistoryPendaftaran history : historyPendaftaranRepository.findAll()) {
            if (history.getIdPendaftar().equals(idProfile)) {
                historyPendaftaranList.add(history);
            }
        }
        return historyPendaftaranList;
    }

    @Override
    public HistoryPendaftaran getHistoryPendaftaran(Integer idHistoryPendaftaran) {
        return historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
    }

    @Override
    public void updateApprovalStatus(int idHistoryPendaftaran, boolean isApproved) {
        HistoryPendaftaran history = historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
        history.setApproved(isApproved);

        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(history.getIdPendaftar());

        if (history.getApproved()) {
            updateStatusPendaftar(pendaftar);
        }

        history.getPendaftar().setRegistered(false);

        historyPendaftaranRepository.save(history);
    }

    private void updateStatusPendaftar(Pendaftar pendaftar) {
        for (Pendaftar p : pendaftarService.getListPendaftar()) {
            if (p.getNik().equals(pendaftar.getNik())) {
                if (!p.isFirstVaccinated() && !p.isSecondVaccinated()) {
                    pendaftar.setFirstVaccinated(true);
                } else if (p.isFirstVaccinated()) {
                    pendaftar.setSecondVaccinated(true);
                }
            }
        }

        if (!pendaftar.isFirstVaccinated() && !pendaftar.isSecondVaccinated()) {
            pendaftar.setFirstVaccinated(true);
        }
    }

    @Override
    public void updateFinishedStatus(int idHistoryPendaftaran, boolean isFinished) {
        HistoryPendaftaran history = historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
        history.setFinished(isFinished);

        if (history.isFinished()) {
            getAllHistoryPendaftaran().removeIf(h -> h == history);
        }

        historyPendaftaranRepository.save(history);
    }

    @Override
    public List<HistoryPendaftaran> getListPendaftaranUnfinished() {
        return historyPendaftaranRepository.findAllByFinishedIs(false);
    }

    @Override
    public void daftarVaksinasi(HistoryPendaftaran historyPendaftaran) {
        String nik = historyPendaftaran.getNik();
        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(nik);
        if (pendaftar != null) {
            if (!pendaftar.isRegistered()){
                pendaftar.setRegistered(true);
                historyPendaftaran.setPendaftar(pendaftar);
                historyPendaftaranRepository.save(historyPendaftaran);
            }
        }
    }
}
