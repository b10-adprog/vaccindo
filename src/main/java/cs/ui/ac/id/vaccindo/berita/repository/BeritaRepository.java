package cs.ui.ac.id.vaccindo.berita.repository;

import cs.ui.ac.id.vaccindo.berita.model.Berita;
import cs.ui.ac.id.vaccindo.berita.model.KategoriBerita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeritaRepository extends JpaRepository<Berita, Integer> {
    @Override
    List<Berita> findAll();
    Berita findBeritaByIdBerita(Integer idBerita);
    void deleteBeritaByIdBerita(Integer idBerita);
    List<Berita> findBeritasByKategoriIs(KategoriBerita kategori);

}
